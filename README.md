# lipa-fg-customScenery-Sources



## What is this?

This repository is an archive of the source files used to generate the new terrain (ws2) for the Aviano Air Base area in FlightGear. For my setup, this is the `data` directory however this can vary based on your setup.

These files, except a small minority, are not process in any way. If you are using this data for whatever reason, you will need to do the required processing yourself (`ogr-decode`, `genapts850`). There are some scripts in various places that may help automate this process. They are based off various sources, FG Wiki, various Git repo's from other people. (Special thanks to colingeniet, creator of the ESPE area scenery for including a bunch in the repo. Go check that scenery out if you havent already!)

## Further Notes:

Yes, I'm well aware that some of the folders are a big mess, have duplicate files, or are even empty. This is my first real attempt at generating anythig using TerraGear, I've learned a lot since I started off back in January of 2023

## Sources

# SRTM-3 Height data

- http://viewfinderpanoramas.org/dem3.html

# Landclass Data

- Corine CLC 2018 - https://land.copernicus.eu/pan-european/corine-land-cover/clc2018
- OpenStreetMap - Used for line based features (Roads, Rivers, Rails etc) - https://openstreetmap.org

# Airport data

- XPlane Scenery Gateway - https://gateway.x-plane.com/ (Some airports have changes made that are not synced to the gateway)
- paolozamp's custom Rivolto scenery for XPlane 12 - https://forums.x-plane.org/index.php?/files/file/86733-_0100_it_fvg_lipi_rivolto_pan_xp12r1/ (Permission has been given for this to be used in FG and licenced under the same terms as the Scenery Gateway.)
